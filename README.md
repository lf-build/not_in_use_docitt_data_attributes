**** This fork docit_data_attributes is created on 03 April 2018 from lf_data_attribute, as the data formats are changed in lf_data_attributes

** Image Name : image: registry.lendfoundry.com/data-attributes **

##### Availalbe End Points:
 
1. Get All Attributes 

    ```
   - GET (/{entityType}/{entityId})   
   ```

2. Get Attribute By Names

    ```
   - GET ({entityType}/{entityId}/names/{*names})   
   ```

3. Get Single Attribute by Name

    ```
   - GET ({entityType}/{entityId}/{name})   
   ```

4. Set Attribute

    ```
   - POST ({entityType}/{entityId}/{name})   
   - Body (object)
   ```

5. Remove Attribute

    ```
   - DELETE ({entityType}/{entityId}/{name})     
   ```
 
#**Configurations**

1. Configuration for the {{configurationservice}}/data-attribute

```
{
  "entityType": {
    "eventName": {
      "entityId": "entityId",
      "ruleName": "rule_Name",
      "ruleVersion": "rule_Verison"
    }
  }
}
```