﻿using System;
using System.Runtime;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Configuration;
using LendFoundry.DataAttributes.Persistence;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.DataAttributes.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
           // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "DataAttributes"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.DataAttributes.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(LendFoundry.DataAttributes.Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, LendFoundry.DataAttributes.Settings.ServiceName);
            services.AddConfigurationService<DataAttributeConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, LendFoundry.DataAttributes.Settings.ServiceName);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);

            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            services.AddTransient<IDataAttributeConfiguration>(p => p.GetService<IConfigurationService<DataAttributeConfiguration>>().Get());
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IDataAttributeRepositoryFactory, DataAttributeRepositoryFactory>();
            services.AddTransient<IDataAttributeRepository, DataAttributeRepository>();
            services.AddTransient<IDataAttributesEngine, DataAttributeEngine>();
            services.AddTransient<IDataAttributesEngineFactory, DataAttributeEngineFactory>();
            services.AddTransient<IDataAttributeListener, DataAttributeListener>();
            services.AddEncryptionHandler();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
 Console.WriteLine(GCSettings.LatencyMode);
            Console.WriteLine(GCSettings.IsServerGC);
            Console.WriteLine(GCSettings.LargeObjectHeapCompactionMode);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DataAttributes Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseDataAttributeListener();
            app.UseMvc();
 

        }
    }
}