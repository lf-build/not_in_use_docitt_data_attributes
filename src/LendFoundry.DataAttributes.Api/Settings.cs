﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.DataAttributes.Api
{
    public static class Settings
    {
        private const string Prefix = "DATA-ATTRIBUTES";

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE", "decision-engine");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "data-attributes");

        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUP", "lookup");

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

    }
}
