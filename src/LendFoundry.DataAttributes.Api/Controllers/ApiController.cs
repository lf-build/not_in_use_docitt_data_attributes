﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.DataAttributes.Api.Controllers
{
    /// <summary>
    /// Represents API controller class.
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="service"></param>
        public ApiController(IDataAttributesEngine service)
        {
            Service = service;
        }

        private IDataAttributesEngine Service { get; }

        private static NoContentResult NoContent => new NoContentResult();

        /// <summary>
        /// Gets all attributes.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(IDictionary<string, object>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
       
        public Task<IActionResult> GetAllAttributes(string entityType, string entityId)
        {
            return ExecuteAsync(async () => Ok(await Service.GetAllAttributes(entityType, entityId)));
        }

        /// <summary>
        /// Gets the attribute by names.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="names">The names.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/names/{*names}")]
            [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
      
        public Task<IActionResult> GetAttributeByNames(string entityType, string entityId, string names)
        {
            return ExecuteAsync(async () => {
                if (string.IsNullOrWhiteSpace(names))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(names));

                names = WebUtility.UrlDecode(names);
                var nameList = SplitNames(names);

                return Ok(await Service.GetAttribute(entityType, entityId, nameList));
            });
        }

        /// <summary>
        /// Gets the attribute.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{name}")]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]   
        public Task<IActionResult> GetAttribute(string entityType, string entityId, string name)
        {
            return ExecuteAsync(async () => Ok(await Service.GetAttribute(entityType, entityId, name)));
        }

        /// <summary>
        /// Sets the attribute by name.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{name}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> SetAttribute(string entityType, string entityId, string name, [FromBody]object value)
        {
           name = WebUtility.UrlDecode(name);
            return await ExecuteAsync(async  () =>
            {
                await Service.SetAttribute(entityType, entityId, name, value);
                return NoContent;
            });
        }

        /// <summary>
        /// Sets the attributes.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> SetAttributeS(string entityType, string entityId, [FromBody]Dictionary<string, object> values)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.SetAttributes(entityType, entityId, values);
                return NoContent;
            });
        }

        /// <summary>
        /// Removes the attribute.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        [HttpDelete("{entityType}/{entityId}/{name}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> RemoveAttribute(string entityType, string entityId, string name)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.RemoveAttribute(entityType, entityId, name);
                return NoContent;
            });
        }

        /// <summary>
        /// Splits the names.
        /// </summary>
        /// <param name="names">The names.</param>
        /// <returns></returns>
        private static List<string> SplitNames(string names)
        {
            return string.IsNullOrWhiteSpace(names)
                ? new List<string>()
                : names.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}