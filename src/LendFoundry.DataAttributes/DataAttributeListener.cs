﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Configuration;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.DataAttributes
{
    public class DataAttributeListener : IDataAttributeListener
    {
        public DataAttributeListener
        (
            ITokenHandler tokenHandler,
            ILoggerFactory loggerFactory,
            IEventHubClientFactory eventHubFactory,
            IConfigurationServiceFactory<DataAttributeConfiguration> configurationDataAtributeFactory,
            ITenantServiceFactory tenantServiceFactory,
            IDecisionEngineClientFactory decisionEngineFactory,
            IDataAttributesEngineFactory engineFactory,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationFactory, ILookupClientFactory lookupServiceFactory)
        {
            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(configurationDataAtributeFactory), configurationDataAtributeFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(engineFactory), engineFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);

            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            ConfigurationDataAttributeFactory = configurationDataAtributeFactory;
            TenantServiceFactory = tenantServiceFactory;
            DecisionEngineFactory = decisionEngineFactory;
            DataAttributesEngineFactory = engineFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
            LookupServiceFactory = lookupServiceFactory;
        }

        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IConfigurationServiceFactory<DataAttributeConfiguration> ConfigurationDataAttributeFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IDecisionEngineClientFactory DecisionEngineFactory { get; }
        private IDataAttributesEngineFactory DataAttributesEngineFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ILookupClientFactory LookupServiceFactory { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);

                tenantService.GetActiveTenants().ForEach(tenant =>
                {
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var eventHub = EventHubFactory.Create(reader);

                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    var configuration = ConfigurationDataAttributeFactory.Create(reader).Get();
                    var engine = DataAttributesEngineFactory.Create(reader, logger, tenantTime);
                    var decisionEngine = DecisionEngineFactory.Create(reader);
                    var lookupService = LookupServiceFactory.Create(reader);

                    if (configuration != null)
                    {
                        var lookupEntries = lookupService.GetLookupEntries("entityTypes");

                        configuration.Values
                                     .SelectMany(entity => entity.Keys)
                                     .Distinct()
                                     .ToList()
                                     .ForEach(eventName =>
                                     {
                                         eventHub.On(eventName, ParseAttributes(configuration, engine, decisionEngine, logger, lookupEntries));
                                         logger.Info($"It was made subscription to EventHub with the Event: #{eventName} for tenant {tenant.Id}");
                                     });
                    }
                    else
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant.Id} , please verify");
                    }

                    eventHub.StartAsync();

                });

                logger.Info("Listening to events");
            }
            catch (Exception ex)
            {
                logger.Error("Connection error listening to events", ex);
                Start();
            }
        }

        private static Action<EventInfo> ParseAttributes(IDataAttributeConfiguration configuration, IDataAttributesEngineExtended engine, IDecisionEngineService decisionEngine, ILogger logger, Dictionary<string, string> lookupEntries)
        {
            return @event =>
            {
                var matches = FindMatchingConfigurations(configuration, @event);
                Parallel.ForEach(matches, ParseAttribute(engine, decisionEngine, logger, @event, lookupEntries));
            };
        }

        private static Action<dynamic> ParseAttribute(IDataAttributesEngineExtended engine, IDecisionEngineService decisionEngine, ILogger logger, EventInfo @event, Dictionary<string, string> lookupEntries)
        {
            return match =>
            {
                try
                {
                    var parameter = new { match.entityType, match.entityId, @event };
                    Dictionary<string, object> result = decisionEngine.Execute<dynamic, Dictionary<string, object>>(match.ruleName, parameter);
                    if (result != null)
                    {
                        var tasks = new List<Task>();
                        foreach (var attribute in result)
                            tasks.Add(engine.SetAttribute(match.entityType, match.entityId, attribute.Key, attribute.Value, lookupEntries));
                        Task.WaitAll(tasks.ToArray());
                    }
                    else
                    {
                        logger.Warn($"No Attributes returned from rule {match.ruleName} for tenant {@event.TenantId}", @event);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error parsing event", ex, new { match, @event });
                }
            };
        }

        private static IEnumerable<dynamic> FindMatchingConfigurations(IDataAttributeConfiguration configuration, EventInfo @event)
        {
            return from entityConfiguration in configuration
                   from eventConfiguration in entityConfiguration.Value
                   where string.Equals(eventConfiguration.Key, @event.Name, StringComparison.InvariantCultureIgnoreCase)
                   select new
                   {
                       entityType = entityConfiguration.Key,
                       entityId = Convert.ToString(InterpolateExtentions.GetPropertyValue(eventConfiguration.Value.EntityId.Replace("{", "").Replace("}", ""), @event)),
                       ruleName = eventConfiguration.Value.RuleName,
                       ruleVersion = eventConfiguration.Value.RuleVersion
                   };
        }

        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
    }
}
