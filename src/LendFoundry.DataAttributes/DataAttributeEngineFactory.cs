﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

using System;
using LendFoundry.Foundation.Lookup.Client;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.DataAttributes
{
    public class DataAttributeEngineFactory : IDataAttributesEngineFactory
    {
        public DataAttributeEngineFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IDataAttributesEngineExtended Create(StaticTokenReader reader, ILogger logger, ITenantTime tenantTime)
        {
            return new DataAttributeEngine
            (
                repository: Provider.GetService<IDataAttributeRepositoryFactory>().Create(reader),
                tenantTime: tenantTime,
                lookup: Provider.GetService<ILookupClientFactory>().Create(reader),
                eventHub: Provider.GetService<IEventHubClientFactory>().Create(reader)
            );
        }
    }
}
