﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.DataAttributes
{
    public static class DataAttributeListenerExtensions
    {
        public static void UseDataAttributeListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IDataAttributeListener>().Start();
        }
    }
}
