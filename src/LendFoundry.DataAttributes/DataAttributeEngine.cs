﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using System;
using LendFoundry.Foundation.Lookup;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using System.Text.RegularExpressions;

namespace LendFoundry.DataAttributes
{
    public class DataAttributeEngine : IDataAttributesEngineExtended
    {
        public DataAttributeEngine(IDataAttributeRepository repository, ITenantTime tenantTime, ILookupService lookup, IEventHubClient eventHub)
        {
            if (repository == null) throw new ArgumentNullException($"{nameof(repository)} cannot be null.");
            if (tenantTime == null) throw new ArgumentNullException($"{nameof(tenantTime)} cannot be null.");
            if (lookup == null) throw new ArgumentNullException($"{nameof(tenantTime)} cannot be null.");

            Repository = repository;
            TenantTime = tenantTime;
            Lookup = lookup;
            EventHub = eventHub;
        }

        private IDataAttributeRepository Repository { get; }
        private ITenantTime TenantTime { get; }
        private ILookupService Lookup { get; }
        private IEventHubClient EventHub { get; }

        public async Task<IDictionary<string, object>> GetAllAttributes(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            entityType = EnsureEntityType(entityType);

            var attributes = await Repository.FindByEntity(entityType, entityId);
            return attributes.ToDictionary(a => a.Name, a => a.Value);
        }

        public async Task<object> GetAttribute(string entityType, string entityId, string name)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));

            entityType = EnsureEntityType(entityType);

            var attribute = await Repository.GetByName(entityType, entityId, name);
            if (attribute == null)
                throw new NotFoundException($"Attribute {name} not found for {entityType}-{entityId}");
            return attribute.Value;
        }

        public async Task<Dictionary<string, object>> GetAttribute(string entityType, string entityId, List<string> names)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (names.Count == 0)
                throw new ArgumentException($"{nameof(names)} is mandatory");
            var attribute = await Repository.GetByNames(entityType, entityId, names);
            if (attribute != null)
                return attribute.ToDictionary(a => a.Name, a => a.Value);
            else
                return null;
        }

        public async Task SetAttribute(string entityType, string entityId, string name, object value, Dictionary<string, string> lookupEntries = null)
        {
            entityType = EnsureEntityTypeAndEntityId(entityType, entityId, lookupEntries);

            EnsureDataAttributeName(name);

            await SaveDataAttribute(entityType, entityId, name, value);
        }

        public async Task SetAttributes(string entityType, string entityId, Dictionary<string, object> values)
        {
            entityType = EnsureEntityTypeAndEntityId(entityType, entityId);

            values.Keys.ToList().ForEach((key) => EnsureDataAttributeName(key));

            await Task.Run(async () =>
            {
                foreach (var item in values)
                {
                    await SaveDataAttribute(entityType, entityId, item.Key, item.Value);
                }
            });
        }
        
        public async Task SetAttribute(string entityType, string entityId, string name, object value)
        {
            entityType = EnsureEntityTypeAndEntityId(entityType, entityId);

            EnsureDataAttributeName(name);

            await SaveDataAttribute(entityType, entityId, name, value);
            
        }

        public async Task RemoveAttribute(string entityType, string entityId, string name)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));

            entityType = EnsureEntityType(entityType);

            var attribute = await Repository.GetByName(entityType, entityId, name);

            if (attribute == null)
                throw new NotFoundException($"Attribute {name} not found.");

            Repository.Remove(attribute);

            await EventHub.Publish($"{UppercaseFirst(entityType)}{UppercaseFirst(name)}{nameof(Events.DataAttributeRemoved)}", new Events.DataAttributeRemoved { Attribute = attribute });

        }

        private void EnsureDataAttributeName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException($"Value cannot be null or whitespace.", nameof(name));

            string pattern = @"^[A-Za-z\d_-]+$";
            Match match = Regex.Match(name, pattern, RegexOptions.IgnoreCase|RegexOptions.Compiled);
            if (!match.Success)
                throw new ArgumentException($"Invalid name.", nameof(name));
        }

        private async Task SaveDataAttribute(string entityType, string entityId, string name, object value)
        {

            var attribute = new DataAttribute
            {
                EntityType = entityType,
                EntityId = entityId,
                Name = name,
                Value = value,
                Time = new TimeBucket(TenantTime.Now)
            };

            await Repository.SetAttribute(attribute);

            await EventHub.Publish($"{UppercaseFirst(entityType)}{UppercaseFirst(name)}{nameof(Events.DataAttributeSet)}", new Events.DataAttributeSet { Attribute = attribute });

        }

        private string EnsureEntityTypeAndEntityId(string entityType, string entityId, Dictionary<string, string> entityTypes = null)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            entityType = EnsureEntityType(entityType, entityTypes);

            return entityType;
        }

        public string EnsureEntityType(string entityType, Dictionary<string, string> entityTypes = null)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");
            entityType = entityType.ToLower();

            if (entityTypes == null)
            {
                entityTypes = Lookup.GetLookupEntry("entityTypes", entityType);
            }

            if (!entityTypes.ContainsKey(entityType))
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }
    }
}
