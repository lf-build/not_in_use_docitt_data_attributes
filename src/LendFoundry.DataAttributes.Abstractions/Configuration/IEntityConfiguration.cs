﻿using System.Collections.Generic;

namespace LendFoundry.DataAttributes.Configuration
{
    public interface IEntityConfiguration : IDictionary<string, EventConfiguration>
    {
    }
}