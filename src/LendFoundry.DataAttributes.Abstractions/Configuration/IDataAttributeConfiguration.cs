﻿using System.Collections.Generic;

namespace LendFoundry.DataAttributes.Configuration
{
    public interface IDataAttributeConfiguration : IDictionary<string, EntityConfiguration>
    {
    }
}