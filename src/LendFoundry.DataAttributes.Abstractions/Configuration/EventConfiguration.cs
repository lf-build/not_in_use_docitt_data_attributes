﻿namespace LendFoundry.DataAttributes.Configuration
{
    public class EventConfiguration : IEventConfiguration
    {
        public string EntityId { get; set; }

        public string RuleName { get; set; }

        public string RuleVersion { get; set; }
    }
}