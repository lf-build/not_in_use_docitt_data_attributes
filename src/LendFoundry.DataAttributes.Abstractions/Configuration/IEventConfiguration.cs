﻿namespace LendFoundry.DataAttributes.Configuration
{
    public interface IEventConfiguration
    {
        string EntityId { get; set; }

        string RuleName { get; set; }

        string RuleVersion { get; set; }
    }
}