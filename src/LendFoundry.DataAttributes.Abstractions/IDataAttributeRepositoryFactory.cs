﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.DataAttributes
{
    public interface IDataAttributeRepositoryFactory
    {
        IDataAttributeRepository Create(ITokenReader reader);
    }
}