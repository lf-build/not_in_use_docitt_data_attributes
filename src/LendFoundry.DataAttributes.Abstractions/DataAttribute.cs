﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.DataAttributes
{
    public class DataAttribute : Aggregate, IDataAttribute
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string Name { get; set; }
        public object Value { get; set; }
        public TimeBucket Time { get; set; }
    }
}