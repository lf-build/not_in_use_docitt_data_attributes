﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.DataAttributes
{
    public interface IDataAttributesEngineExtended : IDataAttributesEngine
    {
        Task SetAttribute(string entityType, string entityId, string name, object value, Dictionary<string, string> lookupEntries = null);
    }
}