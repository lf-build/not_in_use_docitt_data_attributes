﻿namespace LendFoundry.DataAttributes.Events
{
    public class DataAttributeSet
    {
        public IDataAttribute Attribute {get;set;}
    }
}
