﻿namespace LendFoundry.DataAttributes.Events
{
    public class DataAttributeRemoved
    {
        public IDataAttribute Attribute { get; set; }
    }
}
