﻿using System;

namespace LendFoundry.DataAttributes
{
    public static class Settings
    {
        public static string ServiceName { get; } = "data-attributes";
    }
}
