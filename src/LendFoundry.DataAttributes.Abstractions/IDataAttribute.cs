﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.DataAttributes
{
    public interface IDataAttribute : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string Name { get; set; }
        object Value { get; set; }
        TimeBucket Time { get; set; }
    }
}