﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.DataAttributes
{
    public interface IDataAttributeRepository : IRepository<IDataAttribute>
    {
        Task<IEnumerable<IDataAttribute>> FindByEntity(string entityType, string entityId);

        Task<IDataAttribute> GetByName(string entityType, string entityId, string name);
        Task<IList<IDataAttribute>> GetByNames(string entityType, string entityId, List<string> names);

        Task SetAttribute(IDataAttribute dataAttribute);
    }
}