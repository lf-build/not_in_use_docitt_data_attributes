﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.DataAttributes
{
    public interface IDataAttributesEngine
    {
        Task<IDictionary<string, object>> GetAllAttributes(string entityType, string entityId);

        Task<object> GetAttribute(string entityType, string entityId, string name);
        Task<Dictionary<string,object>> GetAttribute(string entityType, string entityId, List<string> names);

        Task SetAttribute(string entityType, string entityId, string name, object value);
        Task SetAttributes(string entityType, string entityId, Dictionary<string, object> values);


        Task RemoveAttribute(string entityType, string entityId, string name);
    }
}
