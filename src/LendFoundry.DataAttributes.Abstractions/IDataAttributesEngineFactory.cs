﻿using LendFoundry.DataAttributes.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.DataAttributes
{
    public interface IDataAttributesEngineFactory
    {
        IDataAttributesEngineExtended Create(StaticTokenReader reader, ILogger logger, ITenantTime tenantTime);
    }
}
