﻿namespace LendFoundry.DataAttributes
{
    public interface IDataAttributeListener
    {
        void Start();
    }
}
