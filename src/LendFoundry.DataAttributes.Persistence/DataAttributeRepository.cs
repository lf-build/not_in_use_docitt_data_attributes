﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Security.Encryption;

namespace LendFoundry.DataAttributes.Persistence
{
    public class DataAttributeRepository : MongoRepository<IDataAttribute, DataAttribute>, IDataAttributeRepository
    {
        static DataAttributeRepository()
        {
            

            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

        }

        public DataAttributeRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService) :
            base(tenantService, configuration, "data-attributes")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(DataAttribute)))
            {
                BsonClassMap.RegisterClassMap<DataAttribute>(map =>
                {
                    map.AutoMap();

                    var type = typeof(DataAttribute);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.MapMember(m => m.Value).SetSerializer(new BsonEncryptor<object, object>(encrypterService));
                    map.SetIgnoreExtraElements(true);
                    map.SetIsRootClass(true);
                });
            }
            var namePerEntity = Builders<IDataAttribute>.IndexKeys
                .Ascending(a => a.TenantId)
                .Ascending(a => a.EntityType)
                .Ascending(a => a.EntityId)
                .Ascending(a => a.Name);

            CreateIndexIfNotExists(nameof(namePerEntity), namePerEntity, true);

            CreateIndexIfNotExists("entityType_entityID", Builders<IDataAttribute>.IndexKeys
    .Ascending(a => a.TenantId)
    .Ascending(a => a.EntityType)
    .Ascending(a => a.EntityId));

        }

        public async Task<IEnumerable<IDataAttribute>> FindByEntity(string entityType, string entityId)
        {
            return await Query.Where(a => a.EntityType == entityType && a.EntityId == entityId).ToListAsync();
        }

        public async Task<IDataAttribute> GetByName(string entityType, string entityId, string name)
        {
            return
                await Query.FirstOrDefaultAsync(
                    a => a.EntityType == entityType && a.EntityId == entityId && a.Name == name);
        }

        public async Task<IList<IDataAttribute>> GetByNames(string entityType, string entityId, List<string> names)
        {
            return
                await Query.Where(a => a.EntityType == entityType && a.EntityId == entityId && names.Contains(a.Name)).ToListAsync();

        }
        public async Task SetAttribute(IDataAttribute dataAttribute)
        {
            dataAttribute.TenantId = TenantService.Current.Id;
            await Collection.UpdateOneAsync(s =>
                    s.TenantId == TenantService.Current.Id &&
                    s.EntityType == dataAttribute.EntityType &&
                    s.EntityId == dataAttribute.EntityId &&
                    s.Name == dataAttribute.Name,
                new UpdateDefinitionBuilder<IDataAttribute>()
                    .Set(a => a.Value, dataAttribute.Value)
                    .Set(a => a.Time, dataAttribute.Time)
                , new UpdateOptions() {IsUpsert = true});
        }
    }
}
