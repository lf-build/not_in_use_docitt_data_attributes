﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.DataAttributes.Client
{
    public interface IDataAttributesClientFactory
    {
        IDataAttributesEngine Create(ITokenReader reader);
    }
}
