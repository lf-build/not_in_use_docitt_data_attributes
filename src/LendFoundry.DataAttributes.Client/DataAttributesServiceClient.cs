﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;

namespace LendFoundry.DataAttributes.Client
{
    public class DataAttributesServiceClient : IDataAttributesEngine
    {
        public DataAttributesServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public Task<IDictionary<string, object>> GetAllAttributes(string entityType, string entityId)
        {
            var rest = new RestRequest("{entityType}/{entityId}", Method.GET);
            rest.AddUrlSegment("entityType", entityType);
            rest.AddUrlSegment("entityId", entityId);            
            return Client.ExecuteAsync<IDictionary<string, object>>(rest);
        }

        public async Task<Dictionary<string, object>> GetAttribute(string entityType, string entityId, List<string> names)
        {
            var rest = new RestRequest("{entityType}/{entityId}/names", Method.GET);
            rest.AddUrlSegment("entityType", entityType);
            rest.AddUrlSegment("entityId", entityId);
            AppendNames(rest, names);
            var result = await Client.ExecuteAsync<Dictionary<string, object>>(rest);
            return new Dictionary<string, object>(result);
        }

        public Task<object> GetAttribute(string entityType, string entityId, string name)
        {
            var rest = new RestRequest("{entityType}/{entityId}/{name}", Method.GET);
            rest.AddUrlSegment("entityType", entityType);
            rest.AddUrlSegment("entityId", entityId);
            rest.AddUrlSegment("name", name);
            return Client.ExecuteAsync<object>(rest);
        }

        public Task RemoveAttribute(string entityType, string entityId, string name)
        {
            var rest = new RestRequest("{entityType}/{entityId}/{name}", Method.DELETE);
            rest.AddUrlSegment("entityType", entityType);
            rest.AddUrlSegment("entityId", entityId);
            rest.AddUrlSegment("name", name);
            return Client.ExecuteAsync<Task>(rest);
        }

        public Task SetAttribute(string entityType, string entityId, string name, object value)
        {
            var rest = new RestRequest("{entityType}/{entityId}/{name}", Method.POST);
            rest.AddUrlSegment("entityType", entityType);
            rest.AddUrlSegment("entityId", entityId);
            rest.AddUrlSegment("name", name);
            var json = JsonConvert.SerializeObject(value);
            rest.AddParameter("application/json", json, ParameterType.RequestBody);
            return Client.ExecuteAsync<Task>(rest);
        }

        private static void AppendNames(IRestRequest request, IReadOnlyList<string> names)
        {
            if (names == null || !names.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < names.Count; index++)
            {
                var tag = names[index];
                url = url + $"/{{name{index}}}";
                request.AddUrlSegment($"name{index}", tag);
            }
            request.Resource = url;
        }

        public Task SetAttributes(string entityType, string entityId, Dictionary<string, object> values)
        {
            var rest = new RestRequest("{entityType}/{entityId}", Method.POST);
            rest.AddUrlSegment("entityType", entityType);
            rest.AddUrlSegment("entityId", entityId);
            var json = JsonConvert.SerializeObject(values);
            rest.AddParameter("application/json", json, ParameterType.RequestBody);
            return Client.ExecuteAsync<Task>(rest);
        }
    }
}
