﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.DataAttributes.Client
{
    public static class DataAttributeClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddDataAttributes(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IDataAttributesClientFactory>(p => new DataAttributeClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IDataAttributesClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDataAttributes(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IDataAttributesClientFactory>(p => new DataAttributeClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IDataAttributesClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDataAttributes(this IServiceCollection services)
        {
            services.AddTransient<IDataAttributesClientFactory>(p => new DataAttributeClientFactory(p));
            services.AddTransient(p => p.GetService<IDataAttributesClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
